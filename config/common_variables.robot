*** Variables ***
${EXTRA_SMALL_SLEEP}    2s
${SMALL_SLEEP}    5s
${MEDIUM_SLEEP}    10s
${LARGE_SLEEP}    15s

${MASTER_TESTBED_PATH}    /config/master_config_data.json


# This are the global variable declared to handle the pybot variables.
${SUT_HOST}    ${EMPTY}
${BROWSER}    ${EMPTY}

# API Config Data
${base_uri}    /api/v1

#sample Site
${SAMPLESITE_URL}    http://dummy.restapiexample.com/
${site_all_emp_emendpoint}    /api/v1/employees
${sample_test_data}    config/sample/sample.json
${site_add_emp_emendpoint}    /api/v1/create
# API endpoints
${authenticate_endpoint}    /api/v1/authenticate



# API Response Codes

${SUCCESS_CODE}    200
${DUPLICATE_OBJECT_CODE}    409
${UNAUTHORIZATION_ACCESS_CODE}    407    
