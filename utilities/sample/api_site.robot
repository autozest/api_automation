*** Settings ***
Documentation    Library to store all the Keywords for APIs related to Sites
Library    RequestsLibrary
Resource   ../rbac/api_rbac.robot
Resource    ../common_utils.robot
Resource    ../api_common_utils.robot
Resource    ../../config/common_variables.robot
Resource    ../common_utils.robot

*** Keywords ***
Set Site API Suite Variable
    ${site_testbed_dict}=    Get Site Test Data
    Set Suite Variable    ${site_testbed_dict}
    
Get All Employee
    [Documentation]    Keyword to Fetch All employee  Info from server.
    Create Session    sample_url     ${SAMPLESITE_URL}
    ${resp}=    Get Request    sample_url    ${site_all_emp_emendpoint}
    Log    ${resp.json()}
    Should Be Equal As Strings    ${resp.status_code}    ${SUCCESS_CODE}    Unauthorized Access Observed ...    
    [Return]    ${resp.json()}

Add New Employee
    [Documentation]    Keyword to Create new record in database\n\n
    [Arguments]    ${generate_random_testdata}    ${site_data_payload}
    Log    ${site_data_payload} 
    
    
    #Generate random test data
    ${site_data_payload}=    Generate Employee Data    ${generate_random_testdata}    ${site_data_payload}  
    Log    ${site_data_payload} 
    
    #Verify testinput data
    ${are_fields_set}=    Verify Employee Input Data    ${site_data_payload}
    Run Keyword If    '${are_fields_set}'=='False'    FAIL    Fields are not properly set    
    
    Create Session    sample_url     ${SAMPLESITE_URL}
    ${TOKEN_HEADER}=    Create Dictionary    Content-Type=application/json   
    ${resp}=    Post Request    sample_url    ${site_add_emp_emendpoint}    data=${site_data_payload}    headers=${TOKEN_HEADER}
    Log    ${resp.json()}    
    Assertion For Successfull Response    ${resp}
    [Return]    ${resp}

    

