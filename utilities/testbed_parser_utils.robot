*** Settings ***
Library    String
Library    Collections
Library    OperatingSystem
Library    SeleniumLibrary
Library    OperatingSystem
Library    RequestsLibrary
Resource    common_utils.robot

*** Keywords ***

Get Home Dir
    [Documentation]    Keyword will return home direcotry
    ${home_dir}=    Set Variable    ${CURDIR}/../
    [return]    ${home_dir}
    
Get Dictionary From Json
    [Documentation]    This keyword is used to get data in form of dictionary from given json file path. 
    [Arguments]    ${file_path}
    ${home_dir}=	Get Home Dir
    ${master_testbed_file}=    Get File    ${home_dir}${file_path}
    ${testbed_file}=    To Json    ${master_testbed_file}
    [Return]    ${testbed_file}

Get Json From Dictionary
    [Documentation]    This keyword is used to convert dictionary to json string.

    [Arguments]    ${data_dict}
    ${json_string}=      Convert To String    ${data_dict}
    ${replace_1}=    Replace String    ${json_string}    '    "
    ${json_format}=    Replace String    ${replace_1}    u"    "
    Log    ${json_format}
    [Return]    ${json_format}
