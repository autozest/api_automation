*** Settings ***
Library    RequestsLibrary
Library    OperatingSystem
Resource    ../testbed_parser_utils.robot
Resource    ../../config/common_variables.robot
Resource    ../api_common_utils.robot

*** Keywords ***

Login To Case Admin From API
    [Documentation]    This keyword is API login post request and verify the status
    [Arguments]    ${user_role}
    ${login_payload_json}=    Get Login Request Data    ${user_role}
    Create Session    caseadmin_url     ${CASEADMIN_URL}    verify=False
    ${resp}=     Post Request    caseadmin_url    ${authenticate_endpoint}    data=${login_payload_json}    headers=${MASTER_TESTBED_DICT['headers']}
    Assertion For Successfull Response    ${resp}
    ${jsondata}=    To Json    ${resp.content}
    ${TOKEN_HEADER}=    Create Dictionary    Authorization=Bearer ${resp.headers['authorization']}    Content-Type=application/json
    Set Global Variable    ${TOKEN_HEADER}
    Log    ${TOKEN_HEADER}

Get Login Request Data
    [Documentation]    This keyword is to create login payload.
    [Arguments]    ${user_role}
    ${login_request_payload}=    Set Variable    {"user_name": "${RBAC_TESTBED_DATA['${user_role}']['username']}","password": "${RBAC_TESTBED_DATA['${user_role}']['password']}","force_login": true}
    ${login_payload_json}=    To Json    ${login_request_payload}
    [Return]    ${login_payload_json}

Logout From Case Admin Using Api
    [Documentation]    This keyword is used to logout from caseadmin application.
    Create Session    caseadmin_url     ${CASEADMIN_URL}    verify=False
    ${resp}=    Get Request    caseadmin_url    ${site_endpoint}    headers=${TOKEN_HEADER}
    Assertion For Successfull Response    ${resp}