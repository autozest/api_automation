*** Settings ***
Documentation    Library to store all the Keywords for APIs
Library    RequestsLibrary
Library    DateTime
Library    String
Library    Collections
Resource    ../config/common_variables.robot
Resource    common_utils.robot
Resource    testbed_parser_utils.robot

*** Keywords ***

Assertion For Successfull Response
    [Arguments]    ${res}
    Should Be Equal As Integers	${res.status_code}	${SUCCESS_CODE}	"Returned invalid response !!"
    Log    Action performed successfully!!

Assertion For Duplicate Object
    [Arguments]    ${res}
	Should Be Equal As Integers	${res.status_code}	${DUPLICATE_OBJECT_CODE}	"Returned invalid response !!"
	Log    Duplicate object cannot be added.
	
Assertion For UnAuthorized Access
    [Arguments]	${user_role}    ${res}
	Should Be Equal As Integers	${res.status_code}	${UNAUTHORIZATION_ACCESS_CODE}	"Returned invalid response !!"
	Log	Action performed successfully with role ${user_role}!!
