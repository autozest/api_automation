*** Settings ***
Library    SeleniumLibrary
Library    RequestsLibrary
Library    Collections
Library    String
Library    OperatingSystem
Resource    testbed_parser_utils.robot
Resource    ../config/common_variables.robot


*** Keywords ***

Import Python Variables
    [Documentation]    This keyword is used to import python variables.
	# Import Variables	${CURDIR}/python_utils.py
	Import Variables    ${CURDIR}/__init__.py    


Get Default Browser
    [Documentation]    Keyword to set default browser for testing unless specified
	${browser}=    Set Variable    ${MASTER_TESTBED_DICT['browser']}
	[return]    ${browser}

Get Master TestBed File
    [Documentation]    This keyword is used to get master testbed file in dictionary.
	${MASTER_TESTBED_DICT}=    Get Dictionary From Json    ${MASTER_TESTBED_PATH}
	Set Global Variable    ${MASTER_TESTBED_DICT

Get json TestBed File
    [Documentation]    This keyword is used to get test testbed file in dictionary.
	${TESTBED_DICT}=    Get Dictionary From Json    ${MASTER_TESTBED_PATH}
	Set Global Variable    ${MASTER_TESTBED_DICT}

Verify Employee Input Data
    [Arguments]    ${pay_load}
    ${is_name_field_set}=    Run Keyword And Return Status    Dictionary Should Contain Key    ${pay_load}    name
    Return From Keyword If    '${is_name_field_set}' == 'False'    False 
    Return From Keyword If    '${pay_load['name']}' == ''    False    
    [return]    True

Generate Employee Data
    [Arguments]    ${flag}    ${pay_load}
    
    Return From Keyword If    '${flag}' == 'False'    ${pay_load}
       
    ${name}=    Generate Random String    4    [String]
    ${surname}=    Generate Random String    4    [String]
    Set To Dictionary    ${pay_load}    name    ${name} ${surname}     
    ${salary}=    Generate Random String    4    [NUMBERS]
    Set To Dictionary    ${pay_load}    salary    ${salary}
    ${age}=    Generate Random String    2    [NUMBERS]
    Set To Dictionary    ${pay_load}    age    ${age}
    
    [Return]    ${pay_load}
