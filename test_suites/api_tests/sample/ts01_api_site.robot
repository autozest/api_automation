*** Settings ***
Library    Collections
Library    String
Library    RequestsLibrary
Resource    ../../../utilities/sample/api_site.robot
Resource    ../../../config/common_variables.robot
Resource    ../../../utilities/testbed_parser_utils.robot
Resource    ../../../utilities/common_utils.robot
Force Tags    Site_Api
Suite Setup    Run Keywords    Import Python Variables

*** Test Cases ***
### TESTS:   http://dummy.restapiexample.com/api/v1/employee
POC [API] Get All Employees
    [Documentation]    Get All the Employees
    [Tags]    Regression    Smoke    API
    Get All Employee
  
### TESTS:   http://dummy.restapiexample.com/api/v1/employees/{id}
    
POC [API] Get Specific Employee With ID
    [Documentation]    Get All the Employees
    [Tags]    Regression    Smoke    API
    ${site_id_data}=    Get Dictionary From Json    ${sample_test_data}
    Log    ${site_id_data} 
    Get Specific Employee    ${site_id_data['emp_2']}
    
### TESTS:   http://dummy.restapiexample.com/api/v1/create
    
POC [API] Add New Employee
    [Documentation]    Add new Employee
    [Tags]    Regression    Smoke    API
    
    # Take data from specific test input file. ${sample_test_data} varialbe contains file path.
    ${site_id_data}=    Get Dictionary From Json    ${sample_test_data}
    Log    ${site_id_data}  
    Add New Employee    False    ${site_id_data['Create_emp_3']}
    
POC [API] Add New Employee And Change Payload At Runtime
    [Documentation]    Add new Employee
    [Tags]    Regression    Smoke    API 
    
    # Take data from specific test input file. ${sample_test_data} varialbe contains file path.
    ${site_id_data}=    Get Dictionary From Json    ${sample_test_data}
    Log    ${site_id_data}    
    Add New Employee    True    ${site_id_data['Create_emp_4']}
    
POC [API] Query Parameter
    [Documentation]    Add new Employee
    [Tags]    Regression    Smoke    API 
    
    # Take data from specific test input file. ${sample_test_data} varialbe contains file path.
    ${site_id_data}=    Get Dictionary From Json    ${sample_test_data_2}
    Log    ${site_id_data}    
    Get All DNA    ${site_id_data['test_1']}
    
Get Requests with Url-Query Parameters
    [Tags]  get
    Create Session  httpbin     http://httpbin.org
    &{params}=   Create Dictionary   key=value     key2=value2
    ${resp}=     Get Request  httpbin  /get    params=${params}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${jsondata}=  To Json  ${resp.content}
    Should Be Equal    ${jsondata['args']}    ${params}

POC [API] Query Parameter2
    [Documentation]    Add new Employee
    [Tags]    Regression    Smoke    API 
    
    # Take data from specific test input file. ${sample_test_data} varialbe contains file path.
    ${site_id_data}=    Get Dictionary From Json    ${sample_test_data_2}
    Log    ${site_id_data}    
    Get All DNA    ${site_id_data['test_2']} 
    
### TESTS:   https://reqres.in/api/users/2   
     
Get Requests with Url Parameters One
    [Tags]  get
    Create Session  httpbin     https://reqres.in
    # Take data from specific test input file. ${sample_test_data} varialbe contains file path.
    ${site_id_data}=    Get Dictionary From Json    ${sample_test_data_2}
    
    ${resp}=     Post Request  httpbin  /api/users/${site_id_data['test_3']['id']} 
    Should Be Equal As Strings  ${resp.status_code}  201
    ${jsondata}=  To Json  ${resp.content}
   
        
Put Requests with Json Data
    [Tags]  put
    Create Session  httpbin     http://httpbin.org
    &{data}=    Create Dictionary   latitude=30.496346  longitude=-87.640356
    ${resp}=     Put Request  httpbin  /put    json=${data}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${jsondata}=  To Json  ${resp.content}
    Should Be Equal    ${jsondata['data']}    json=${data}
 
    
Post With Unicode Data
    [Tags]  post
    Create Session  httpbin  http://httpbin.org    debug=3
    &{data}=  Create Dictionary  name=度假村
    &{headers}=  Create Dictionary  Content-Type=application/x-www-form-urlencoded
    ${resp}=  Post Request  httpbin  /post  data=${data}  headers=${headers}
    Dictionary Should Contain Value  ${resp.json()['form']}  度假村

Post Request With Unicode Data
    [Tags]  post
    Create Session  httpbin  http://httpbin.org    debug=3
    &{data}=  Create Dictionary  name=度假村
    &{headers}=  Create Dictionary  Content-Type=application/x-www-form-urlencoded
    ${resp}=  Post Request  httpbin  /post  data=${data}  headers=${headers}
    Dictionary Should Contain Value  ${resp.json()['form']}  度假村

Post Request With Binary Data in Dictionary
    [Tags]  post
    Create Session  httpbin  http://httpbin.org    debug=3
    ${file_data}=  Get Binary File  ${sample_test_data_3}
    &{data}=  Create Dictionary  name=${file_data.strip()}
    &{headers}=  Create Dictionary  Content-Type=application/x-www-form-urlencoded
    ${resp}=  Post Request  httpbin  /post  data=${data}  headers=${headers}
    Log  ${resp.json()['form']}
    Should Contain  ${resp.json()['form']['name']}  \u5ea6\u5047\u6751


    