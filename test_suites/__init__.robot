*** Settings ***
Resource    ../utilities/common_utils.robot

Suite Setup    Run Keywords
...    Import Python Libraries
...    Import Python Variables
...    Get Master TestBed File
...    Get Rbac Test Data
...    Get SUT URL